package com.boyang.message;

import com.boyang.message.dao.UserDao;
import com.boyang.message.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class MessageApplicationTests {
    @Autowired
    private UserDao userDao;
    @Test
    void contextLoads() {
        List<User> userList = new ArrayList<>();
        User a = new User();
        a.setId(1);
        a.setMoney(BigDecimal.ONE);
        userList.add(a);

        User b = new User();
        b.setId(2);
        b.setMoney(BigDecimal.ONE);
        userList.add(b);
        //批量更新数据 数据库链接+：&allowMultiQueries=true
        userDao.updateUserList(userList);
    }

}
