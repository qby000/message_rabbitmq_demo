package com.boyang.message.service;

import com.boyang.message.entity.User;

public interface UserService {
    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    int insertUser(User user);
}
