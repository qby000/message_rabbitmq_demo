package com.boyang.message.service;

import com.boyang.message.entity.User;

import java.math.BigDecimal;
import java.util.List;

public interface RedPacketService {
    /**
     * 发红包
     *
     * @param user
     * @return
     */
    List<BigDecimal> sendRedPacket(User user);

    /**
     * 开启自动发包
     *
     * @param number
     * @return
     */
    boolean startRedPacket(Integer number);
}
