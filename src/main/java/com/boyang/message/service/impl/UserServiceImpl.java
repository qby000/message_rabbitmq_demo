package com.boyang.message.service.impl;

import com.boyang.message.controller.RabbitController;
import com.boyang.message.entity.User;
import com.boyang.message.rabbitmq.RabbitConfig;
import com.boyang.message.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/06 15:52
 */
@Service
public class UserServiceImpl implements UserService, RabbitTemplate.ConfirmCallback {
    private final Logger logger = LoggerFactory.getLogger(RabbitController.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    @Override
    public int insertUser(User user) {
        int result = 0;
        try {
            String id = String.valueOf(System.currentTimeMillis());
            CorrelationData correlationId = new CorrelationData(id);
            //param1：交换器 param2：ROUTINGKEY键 param3：内容 param4：编号唯一
            rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_B, RabbitConfig.ROUTINGKEY_B, user, correlationId);
            result = 1;
        } catch (Exception e) {
            logger.error("消息队列发生异常： ", e.fillInStackTrace());
            result = -1;
        }
        return result;
    }

    /**
     * 回调构造方法
     */
    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setConfirmCallback(new RabbitController());
    }

    /**
     * 回调确认信息
     *
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        System.out.println("消息编号:" + correlationData.getId());
        if (ack) {
            logger.info("消息发送确认成功");
        } else {
            logger.info("消息发送确认失败:" + cause);
        }
    }
}
