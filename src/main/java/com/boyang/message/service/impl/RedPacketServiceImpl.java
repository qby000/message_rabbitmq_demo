package com.boyang.message.service.impl;

import com.boyang.message.dao.RobMoneyDao;
import com.boyang.message.dao.UserDao;
import com.boyang.message.entity.RobMoney;
import com.boyang.message.entity.User;
import com.boyang.message.rabbitmq.RabbitConfig;
import com.boyang.message.service.RedPacketService;
import com.boyang.message.util.DistributeMoney;
import com.boyang.message.util.hongbao.RedEnvelop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/06 15:59
 */
@Service
public class RedPacketServiceImpl implements RedPacketService, RabbitTemplate.ConfirmCallback {
    private final Logger logger = LoggerFactory.getLogger(RedPacketServiceImpl.class);
    @Autowired
    private UserDao userDao;
    @Autowired
    private RobMoneyDao robMoneyDao;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public List<BigDecimal> sendRedPacket(User user) {
        List<User> userList = userDao.selectUserList();
        Random random = new Random();
        User checkUser = userList.get(random.nextInt(userList.size()));
        if (checkUser == null) {
            return null;
        }
        BigDecimal userMoney = checkUser.getMoney();
        BigDecimal sendMoney = user.getMoney();
        if (userMoney.compareTo(sendMoney) == -1) {
            return null;
        }
        if (user.getCount() == 0) {
            return null;
        }
        if (user.getMoney().compareTo(BigDecimal.ZERO) == 0) {
            return null;
        }
        RedEnvelop redPacket = new RedEnvelop(user.getCount(), userMoney.floatValue());
        //分配红包列表
        List<BigDecimal> redPacketList = new ArrayList<>();
        for (int i = 0; i < redPacket.getCount(); i++) {
            BigDecimal distributeMoney = redPacket.nextRed();
            redPacketList.add(distributeMoney);
        }
        //更新用户金额
        BigDecimal yuE = userMoney.subtract(sendMoney);
        checkUser.setMoney(yuE);
        userDao.updateUser(checkUser);
        return redPacketList;
    }

    /**
     * 开启自动发包
     *
     * @param number
     * @return
     */
    @Override
    public boolean startRedPacket(Integer number) {
        //isSend状态存入数据库 可以控制方法执行开关
        boolean isSend = false;
        if (number == 1) {
            isSend = true;
        }
        List<User> userList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.00");
        while (isSend) {
            try {
                userList = userDao.selectUserList();
                //随机金额保留两位小数
                String moneyStr = df.format(1 + Math.random() * 99);
                double money = new Double(moneyStr);
                //随机红包数量
                int count = (int) (1 + Math.random() * 20);
                //随机生成红包
                List<Double> redPackList = DistributeMoney.distribute(money, count);
                Thread.sleep(3000);
                RobMoney robMoney = new RobMoney();
                robMoney.setRedPackList(redPackList);
                robMoney.setUserList(userList);
                robMoney.setCount(count);
                String id = System.currentTimeMillis() + UUID.randomUUID().toString();
                CorrelationData correlationId = new CorrelationData(id);
                //param1：交换器 param2：ROUTINGKEY键 param3：内容 param4：编号唯一
                rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_C, RabbitConfig.ROUTINGKEY_C, robMoney, correlationId);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return isSend;
    }

    /**
     * 回调构造方法
     */
    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setConfirmCallback(new RedPacketServiceImpl());
    }

    /**
     * 回调确认信息
     *
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        System.out.println("消息编号:" + correlationData.getId());
        if (ack) {
            logger.info("消息发送确认成功");
        } else {
            logger.info("消息发送确认失败:" + cause);
        }
    }
}
