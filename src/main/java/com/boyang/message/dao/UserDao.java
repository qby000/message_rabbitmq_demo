package com.boyang.message.dao;

import com.boyang.message.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/06 15:51
 */
public interface UserDao {
    /**
     * 通过id查询用户信息
     *
     * @param id
     * @return
     */
    User selectUserById(Integer id);

    /**
     * 更新用户信息
     *
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 查询用户列表
     *
     * @return
     */
    List<User> selectUserList();

    /**
     * 新增用户信息
     *
     * @param user
     * @return
     */
    int insertUser(User user);

    /**
     * 批量用户更新
     * @param userList
     * @return
     */
    int updateUserList(@Param("userList") List<User> userList);

}
