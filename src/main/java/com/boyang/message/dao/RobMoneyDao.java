package com.boyang.message.dao;

import com.boyang.message.entity.RobMoney;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/19 9:46
 */
public interface RobMoneyDao {
    /**
     * 新增抢包记录
     *
     * @param robMoney
     * @return
     */
    int insertUserRobMoney(RobMoney robMoney);

    /**
     * 批量插入抢包记录
     * @param robMoneyList
     * @return
     */
    int insertUserRobMoneyList(@Param("robMoneyList")List<RobMoney> robMoneyList);
}
