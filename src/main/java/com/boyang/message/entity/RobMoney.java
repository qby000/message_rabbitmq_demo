package com.boyang.message.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @describe: 抢包记录
 * @author: yang
 * @date: 2020/01/19 9:44
 */
public class RobMoney implements Serializable {
    private Integer robId;

    private Integer userId;

    private String nickName;

    private BigDecimal robMoney;

    private Date createTime;

    @Override
    public String toString() {
        return "RobMoney{" +
                "robId=" + robId +
                ", userId=" + userId +
                ", nickName='" + nickName + '\'' +
                ", robMoney=" + robMoney +
                ", createTime=" + createTime +
                '}';
    }

    /**
     * 红包列表
     */
    private List<Double> redPackList;

    /**
     * 抢包用户列表
     */
    private List<User> userList;

    /**
     * 红包数量
     */
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Double> getRedPackList() {
        return redPackList;
    }

    public void setRedPackList(List<Double> redPackList) {
        this.redPackList = redPackList;
    }

    public Integer getRobId() {
        return robId;
    }

    public void setRobId(Integer robId) {
        this.robId = robId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public BigDecimal getRobMoney() {
        return robMoney;
    }

    public void setRobMoney(BigDecimal robMoney) {
        this.robMoney = robMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
