package com.boyang.message.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @describe: 用户
 * @author: yang
 * @date: 2020/01/06 15:50
 */
public class User implements Serializable {
    private Integer id;

    private String nickName;

    private String headPhoto;

    private BigDecimal money;

    private Date createTime;

    private Integer count;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", headPhoto='" + headPhoto + '\'' +
                ", money=" + money +
                ", createTime=" + createTime +
                ", count=" + count +
                '}';
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
