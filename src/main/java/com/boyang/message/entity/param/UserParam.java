package com.boyang.message.entity.param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/06 16:27
 */
public class UserParam {
    private String userId;
    private String nickName;
    private String headPhoto;
    private List<BigDecimal> moneyList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public List<BigDecimal> getMoneyList() {
        return moneyList;
    }

    public void setMoneyList(List<BigDecimal> moneyList) {
        this.moneyList = moneyList;
    }
}
