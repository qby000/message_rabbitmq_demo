package com.boyang.message.controller;

import com.boyang.message.entity.User;
import com.boyang.message.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe: 用户
 * @author: yang
 * @date: 2020/01/13 18:03
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    @PostMapping("/add")
    public int insertUser(User user) {
        return userService.insertUser(user);
    }
}
