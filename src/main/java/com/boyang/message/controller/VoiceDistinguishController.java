package com.boyang.message.controller;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @describe: 腾讯云/语音识别
 * @author: yang
 * @date: 2020/01/04 9:10
 */
@RestController
@RequestMapping("/voiceDistinguish")
public class VoiceDistinguishController {
    private static Logger logger = LoggerFactory.getLogger(VoiceDistinguishController.class);
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private static final String APP_ID = "1257935352";
    private static final String SECTET_Id = "AKIDvhDVYNyjFWYKqAp4cvzrTq2yf9NRkp9G";
    private static final String SECRET_KEY = "1maFj04weT1OVMyNWXIziOyV5aCygSCs";
    private static final String CALLBACK_URL = "http://106.54.46.81:5948/message/voiceDistinguish/callback";

    /**
     * 语音识别 /录音文件
     *
     * @param voicePath 文件地址
     * @return
     */
    @GetMapping("/uploadVoice")
    public String getStr(String voicePath) {
        Map<String, String> map = new HashMap<>();
        map.put("projectid", "0");
        map.put("secretid", SECTET_Id);
        map.put("sub_service_type", "0");
        map.put("engine_model_type", "8k_0");
        map.put("res_text_format", "0");
        map.put("res_type", "0");
        map.put("callback_url", CALLBACK_URL);
        map.put("channel_num", "1");
        map.put("source_type", "1");
        map.put("timestamp", String.valueOf(System.currentTimeMillis() / 1000));
        map.put("expired", String.valueOf(System.currentTimeMillis() / 1000 + 1000));
        Random random = new Random();
        map.put("nonce", random.nextInt(100000) + "");

        //网址
        String host = "aai.qcloud.com";
        //拼接请求链接
        String requestUrl = "https://" + getRequestUrl(map);
        //加密字段
        String addPwdWord = "POST" + getRequestUrl(map);
        //HmacSha1 算法进行加密处理
        String authorization = getHMACSHA1(addPwdWord);
        logger.info("请求链接：{}" + requestUrl);
        String contentType = "application/octet-stream";

        //返回结果
        String result = null;
        File file = new File(voicePath);
        int contentLength = (int) file.length();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fileInputStream);
            byte[] bytes = new byte[contentLength];
            bis.read(bytes);
            //发送post请求
            result = sendPostVoice(requestUrl, host, authorization, contentType, bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 语音识别回调
     *
     * @param code
     * @param message
     * @param requestId
     * @param appId
     * @param projectId
     * @param audioUrl
     * @param text
     * @param audioTime
     * @throws Exception
     */
    @PostMapping("/callback")
    public void callBack(@RequestParam(name = "code", required = false) String code,
                         @RequestParam(name = "message", required = false) String message,
                         @RequestParam(name = "requestId", required = false) String requestId,
                         @RequestParam(name = "appid", required = false) String appId,
                         @RequestParam(name = "projectid", required = false) String projectId,
                         @RequestParam(name = "audioUrl", required = false) String audioUrl,
                         @RequestParam(name = "text", required = false) String text,
                         @RequestParam(name = "audioTime", required = false) String audioTime) throws Exception {
        if ("0".equals(code) && "成功".equals(message)) {
            logger.info(code);
            logger.info(message);
            logger.info(requestId);
            logger.info(appId);
            logger.info(projectId);
            logger.info(audioUrl);
            logger.info("text:" + text);
            //回调识别内容
            logger.info("text utg-8:" + URLDecoder.decode(text, "UTF-8"));
            logger.info(audioTime);
        }
    }


    /**
     * 发送post请求
     *
     * @param requestUrl
     * @param host
     * @param authorization
     * @param contentType
     * @param bytes
     */
    private String sendPostVoice(String requestUrl, String host, String authorization, String contentType, byte[] bytes) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(requestUrl);
        String result = "";
        httpPost.setHeader("Host", host);
        httpPost.setHeader("Authorization", authorization);
        httpPost.setHeader("Content-Type", contentType);
        httpPost.setEntity(new ByteArrayEntity(bytes));

        try {
            HttpResponse httpResponse = httpClient.execute(httpPost);
            //请求服务器成功，做相应处理
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                InputStream is = httpResponse.getEntity().getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                is.close();
                result = sb.toString();
                logger.info("服务器请求成功结果：{}", result);
            } else {
                logger.info("服务器请求失败!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 加密字段
     * HmacSha1 算法进行加密处理
     *
     * @param str
     * @return
     */
    private String getHMACSHA1(String str) {
        byte[] result = null;
        try {
            SecretKeySpec signInKey = new SecretKeySpec(SECRET_KEY.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signInKey);
            byte[] rawHmac = mac.doFinal(str.getBytes());
            result = Base64.encodeBase64(rawHmac);
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        } catch (InvalidKeyException e) {
            System.err.println(e.getMessage());
        }
        if (null != result) {
            return new String(result);
        } else {
            return null;
        }
    }

    /**
     * 封装请求url
     *
     * @param map
     * @return
     */
    private String getRequestUrl(Map<String, String> map) {
        StringBuilder sb = new StringBuilder("aai.qcloud.com/asr/v1/");
        sb.append(APP_ID);
        // key 要按照字典排序
        sb.append(MessageFormat.format("?callback_url={0}", map.get("callback_url")));
        sb.append(MessageFormat.format("&channel_num={0}", map.get("channel_num")));
        sb.append(MessageFormat.format("&engine_model_type={0}", map.get("engine_model_type")));
        sb.append(MessageFormat.format("&expired={0}", map.get("expired")));
        sb.append(MessageFormat.format("&nonce={0}", map.get("nonce")));
        sb.append(MessageFormat.format("&projectid={0}", map.get("projectid")));
        sb.append(MessageFormat.format("&res_text_format={0}", map.get("res_text_format")));
        sb.append(MessageFormat.format("&res_type={0}", map.get("res_type")));
        sb.append(MessageFormat.format("&secretid={0}", map.get("secretid")));
        sb.append(MessageFormat.format("&source_type={0}", map.get("source_type")));
        sb.append(MessageFormat.format("&sub_service_type={0}", map.get("sub_service_type")));
        sb.append(MessageFormat.format("&timestamp={0}", map.get("timestamp")));
        return sb.toString();
    }
}
