package com.boyang.message.controller;

import com.boyang.message.dao.RobMoneyDao;
import com.boyang.message.dao.UserDao;
import com.boyang.message.entity.RobMoney;
import com.boyang.message.entity.User;
import com.boyang.message.rabbitmq.RabbitConfig;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/13 18:14
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUE_C)
@Transactional
public class RedpacketReceiverB {
    private final Logger logger = LoggerFactory.getLogger(RedpacketReceiverB.class);
    @Autowired
    private UserDao userDao;
    @Autowired
    private RobMoneyDao robMoneyDao;

    @RabbitHandler
    public void process(RobMoney robMoney, @Headers Map<String, Object> headers, Channel channel) {
        logger.info("接收B红包列表：{}" + robMoney.getRedPackList());
        logger.info("接收B红包数量：{}" + robMoney.getCount());
        //logger.info("接收B用户列表：{}" + robMoney.getUserList());
        //用户列表
        List<User> userList = robMoney.getUserList();
        //红包数量
        Integer count = robMoney.getCount();
        //红包列表
        List<Double> redPacketList = robMoney.getRedPackList();

        List<RobMoney> robMoneyList = new ArrayList<>();
        Collections.shuffle(userList);
        for (int i = 0; i < count; i++) {
            RobMoney rm = new RobMoney();
            BigDecimal fenMoney = new BigDecimal(redPacketList.get(i).toString());
            Integer userId = userList.get(i).getId();
            String nickName = userList.get(i).getNickName();
            Date time = new Date();
            rm.setCreateTime(time);
            rm.setNickName(nickName);
            rm.setRobMoney(fenMoney);
            rm.setUserId(userId);
            robMoneyList.add(rm);
        }
        int result = robMoneyDao.insertUserRobMoneyList(robMoneyList);
        if (result > 0) {
            List<User> users = new ArrayList<>();
            for (RobMoney rm : robMoneyList) {
                User user = userDao.selectUserById(rm.getUserId());
                if (user != null) {
                    BigDecimal yuE = user.getMoney();
                    BigDecimal andAdd = yuE.add(rm.getRobMoney());
                    user.setMoney(andAdd);
                    //userDao.updateUser(user);
                    users.add(user);
                }
            }
            userDao.updateUserList(users);

            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            try {
                channel.basicAck(deliveryTag, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
