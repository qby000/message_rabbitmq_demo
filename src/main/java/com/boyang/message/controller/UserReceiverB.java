package com.boyang.message.controller;

import com.boyang.message.dao.UserDao;
import com.boyang.message.entity.User;
import com.boyang.message.rabbitmq.RabbitConfig;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/13 18:14
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUE_B)
public class UserReceiverB {
    private final Logger logger = LoggerFactory.getLogger(UserReceiverB.class);
    @Autowired
    private UserDao userDao;

    @RabbitHandler
    @Transactional
    public void process(User user, @Headers Map<String, Object> headers, Channel channel) {
        logger.info("接收B内容：{}" + user);
        user.setCreateTime(new Date());
        int result = userDao.insertUser(user);
        if (result > 0) {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            try {
                channel.basicAck(deliveryTag, false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
