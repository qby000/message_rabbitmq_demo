package com.boyang.message.controller;

import com.boyang.message.entity.User;
import com.boyang.message.service.RedPacketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @describe:
 * @author: yang
 * @date: 2020/01/06 15:48
 */
@RestController
@RequestMapping("/redPacket")
public class RedPacketController {
    @Autowired
    private RedPacketService redPacketService;

    /**
     * 发红包列表
     *
     * @param user
     * @return
     */
    @PostMapping("/sendRedPacket")
    public List<BigDecimal> sendRedPacket(@RequestBody User user) {
        return redPacketService.sendRedPacket(user);
    }


    /**
     * 开启关闭自动抢发包
     *
     * @param number
     * @return
     */
    @GetMapping("/startRedPacket")
    public boolean startRedPacket(Integer number) {
        return redPacketService.startRedPacket(number);
    }

}
