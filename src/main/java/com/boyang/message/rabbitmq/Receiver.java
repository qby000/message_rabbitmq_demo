package com.boyang.message.rabbitmq;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 接受者
 */
@Component
@RabbitListener(queues = RabbitConfig.QUEUE_A)
public class Receiver {
    @RabbitHandler
    public void process(String content, @Headers Map<String, Object> headers, Channel channel) {
        System.out.println("接收者： " + content);
        try {
            Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
            channel.basicAck(deliveryTag, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
