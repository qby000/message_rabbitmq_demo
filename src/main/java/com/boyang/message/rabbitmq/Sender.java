package com.boyang.message.rabbitmq;

import com.boyang.message.controller.RabbitController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 发送者
 */
@Component
public class Sender implements RabbitTemplate.ConfirmCallback {
    private final Logger logger = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String context) {
        logger.info("发送者 : " + context);
        String id = String.valueOf(System.currentTimeMillis());
        CorrelationData correlationId = new CorrelationData(id);
        //param1：交换器 param2：ROUTINGKEY键 param3：内容 param4：编号唯一
        rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_A, RabbitConfig.ROUTINGKEY_A, context, correlationId);
    }

    /**
     * 定制rabbitMQ模板
     * ConfirmCallback接口用于实现消息发送到RabbitMQ交换器后接收消息成功回调
     * ReturnCallback接口用于实现消息发送到RabbitMQ交换器，但无相应队列与交换器绑定时的回调  即消息发送不到任何一个队列中回调
     *
     * @return
     */
    @PostConstruct
    public void initRabbitTemplate() {
        rabbitTemplate.setConfirmCallback(new RabbitController());
    }

    /**
     * 回调确认信息
     *
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        logger.info("消息编号:" + correlationData.getId());
        if (ack) {
            logger.info("消息发送确认成功");
        } else {
            logger.info("消息发送确认失败:" + cause);
        }
    }

}
