package com.boyang.message.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 红包分配金额
 * 金额不小于1 money
 * 数量不超过50 count
 * yang
 */
public class DistributeMoney {
    /**
     * 分配金额
     *
     * @param money 金额/金额不小于1
     * @param count 数量/数量不超过50
     * @return
     */
    public static List<Double> distribute(Double money, Integer count) {
        List<Double> list = new ArrayList<>();
        //发包数大于0
        if (count > 0) {
            //金额不小于1
            if (money < 1) {
                return list;
            }
            //数目不超过50
            if (count > 50) {
                return list;
            }

            //平均值
            Double average = money / count;
            //随机生成数 >0 且 <平均值的数字
            Double random = getRandomNumber(average);
            //double 保留2位小数
            DecimalFormat df = new DecimalFormat("#.00");
            //发包个数偶数
            if (count % 2 == 0) {
                Double total = 0.00;
                for (int i = 0; i < count; i++) {
                    Double a = 0.00;
                    if (i % 2 == 0) {
                        //偶数平均数+ 随机生成数
                        String number = df.format(average + random);
                        a = Double.valueOf(number);
                        list.add(a);
                    } else {
                        //奇数平均数- 随机生成数
                        String number = df.format(average - random);
                        a = Double.valueOf(number);
                        list.add(a);
                        random = getRandomNumber(average);
                    }
                    String number = df.format(total + a);
                    total = Double.valueOf(number);
                }
                //发包总金额
                BigDecimal moneyB = new BigDecimal(money.toString());
                //累计金额
                BigDecimal totalB = new BigDecimal(total + "");
                BigDecimal firstB = new BigDecimal(list.get(0).toString());
                //最后一个数替换第一个数
                BigDecimal endB = moneyB.subtract(totalB).add(firstB);
                list.set(0, endB.doubleValue());

                // 发包数奇数
            } else {
                double total = 0;
                //发包数首先减一个
                for (int i = 0; i < count - 1; i++) {
                    Double a = 0.00;
                    if (i % 2 == 0) {
                        //偶数平均数+ 随机生成数
                        String number = df.format(average + random);
                        a = Double.valueOf(number);
                        list.add(a);
                    } else {
                        //奇数平均数- 随机生成数
                        String number = df.format(average - random);
                        a = Double.valueOf(number);
                        list.add(a);
                        random = getRandomNumber(average);
                    }
                    String number = df.format(total + a);
                    total = Double.valueOf(number);
                }
                //发包总金额
                BigDecimal moneyB = new BigDecimal(money.toString());
                //发包数减一 累计金额
                BigDecimal totalB = new BigDecimal(total + "");
                //最后一个数
                BigDecimal endB = moneyB.subtract(totalB);
                list.add(endB.doubleValue());
            }
            Collections.shuffle(list);
        }
        return list;
    }

    /**
     * 生成随机两位数
     *
     * @param average 数字
     * @return
     */
    private static Double getRandomNumber(double average) {
        DecimalFormat df = new DecimalFormat("#.00");
        Double r = new Double(df.format(Math.random() * average));
        // 随机生成 0.00 递归
        if (r == 0.00) {
            getRandomNumber(average);
        }
        return r;
    }

    public static void main(String[] args) throws Exception {
        //测试随机生成1000个包
        for (int j = 0; j < 1000; j++) {
            //发包金额
            DecimalFormat df = new DecimalFormat("#.00");
            double money =new Double(df.format(1 + Math.random() * 99));
            int count = (int) (1 + Math.random() * 20);
            //Double money = new Double(df.format(66));
            //数量
            //int count = 19;
            List<Double> distribute = distribute(money, count);
            BigDecimal number = BigDecimal.ZERO;
            for (int i = 0; i < distribute.size(); i++) {
                System.out.print(distribute.get(i) + " ");
                BigDecimal iB = new BigDecimal(distribute.get(i) + "");
                number = number.add(iB);
            }
            System.out.println("总金额：" + number);
            //测试 分配的总金额 是否等于 发包总金额
            if (money != number.doubleValue()) {
                throw new Exception();
            }
        }
    }
}
