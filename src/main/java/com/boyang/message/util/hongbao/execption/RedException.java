package com.boyang.message.util.hongbao.execption;

/**
 * 红包异常
 * @author yang
 * @Date 2019年11月26日
 */
public class RedException extends RuntimeException {

	private static final long serialVersionUID = -8147835444665555431L;
	
	public RedException(String msg){
		super(msg);
	}

}
