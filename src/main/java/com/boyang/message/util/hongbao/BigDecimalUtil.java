package com.boyang.message.util.hongbao;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * BigDecimalUtil
 * @author yang
 * @Date 2019年11月26日
 */
public class BigDecimalUtil {
	
	/**
	 * �ӷ�
	 */
	public static BigDecimal add(BigDecimal b1, BigDecimal b2){
		 return b1.add(b2, MathContext.DECIMAL128);
	}
	
	/**
	 * ����
	 */
	public static BigDecimal subtract(BigDecimal b1, BigDecimal b2){
		 return b1.subtract(b2, MathContext.DECIMAL128);
	}
	
	/**
	 * �˷�
	 */
	public static BigDecimal multiply(BigDecimal b1, BigDecimal b2){
		 return b1.multiply(b2, MathContext.DECIMAL128);
	}

	/**
	 * ����
	 */
	public static BigDecimal divide(BigDecimal b1, BigDecimal b2){
		 return b1.divide(b2, MathContext.DECIMAL128);
	}
}
